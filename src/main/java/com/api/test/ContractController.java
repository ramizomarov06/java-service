package com.api.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ControllerAdvice;

@RestController
@RequestMapping("/contracts")
public class ContractController {

    @Autowired
    private getContractDetailsByEmployerTin yourService;

    @GetMapping("/{employerTin}")
    public String getContractDetailsByEmployerTin(@PathVariable int employerTin) {
        return yourService.getTinDetail(employerTin);
    }
}