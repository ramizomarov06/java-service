package com.api.test;

import org.springframework.stereotype.Component;

import java.sql.*;
import org.json.JSONArray;
import org.json.JSONObject;

@Component // Add this annotation to make it a Spring bean
public class getContractDetailsByEmployerTin {
    public String getTinDetail(int employerTin) {
        String jdbcUrl = "jdbc:mysql://144.91.94.98:3306/messages";
        String username = "root";
        String password = "Ramiz@Omarov";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            // Prepare the SQL query
            String sqlQuery = "SELECT * FROM messages.emas_contracts WHERE employer_tin = ?";

            try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
                statement.setInt(1, employerTin);

                // Execute the query
                ResultSet resultSet = statement.executeQuery();

                // Обработайте результат
                JSONArray resultArray = new JSONArray(); // Создайте массив для хранения результатов

                while (resultSet.next()) {
                    // Создайте объект JSON для каждой строки в результате
                    JSONObject contractObject = new JSONObject();

                    // Добавьте необходимые поля в объект JSON
                    contractObject.put("state", resultSet.getInt("state"));
                    contractObject.put("contractStatus", resultSet.getInt("contract_status"));
                    contractObject.put("validity", resultSet.getInt("validity"));
                    contractObject.put("employerTin", resultSet.getInt("employer_tin"));

                    // Добавьте объект JSON в массив результатов
                    resultArray.put(contractObject);
                }

                // Верните JSON-строку в качестве результата
                return resultArray.toString();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "{\"error\": \"Error occurred while retrieving contract details.\"}";
    }
}
