package com.api.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.api.test") // Add this line to scan the package
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
